from PIL import Image
from PIL import ImageDraw
from PIL import ImageFont


tiltModifier = 400
innerBorder = 50
titleSize = 70


def generatePage(fileToCreate, text, title, unconventional, tmp):

    # --== SET UP SOME IMPORTANT VARIABLES ==--

    img = Image.new('RGB', (1240, 1754), 'Black')

    draw = ImageDraw.Draw(img)

    font = ImageFont.truetype('font/Font.ttf', size=100)

    # --== START GENERATING THE DIAGONAL LINES AND TEXT BOX ==--

    for i in range(1, int((img.height / 80))):
        if unconventional:
            draw.line((-25, i*200, img.size[0]+25, (-img.size[1]+tiltModifier)+i*200), fill="Orange", width=60)
        else:
            draw.line((-25, i * 200, img.size[0] + 25, (-img.size[1] + tiltModifier) + i * 200), fill="Gray", width=60)

    draw.rectangle((innerBorder, innerBorder, img.size[0]-innerBorder, img.size[1]-innerBorder), fill="White", outline="Black", width=10)

    # --== FINISH GENERATING THE DIAGONAL LINES AND TEXT BOX ==--

    # --== START GENERATING THE TITLE ==--

    titleFont = ImageFont.truetype('font/Font.ttf', size=titleSize)
    titleActualSize = titleFont.getsize(title, stroke_width=1)
    draw.text(((img.size[0]/2)-(titleActualSize[0]/2), innerBorder+20), text=title, fill="Black", font=titleFont, spacing=5, stroke_width=1)

    # --== FINISH GENERATING THE TITLE ==--

    # --== START GENERATING THE ACTUAL TEXT-BLOCK ==--

    first_test = font.getsize_multiline(text, spacing=4, stroke_width=2)
    width = first_test[0]
    heigth = first_test[1]
    font_size = font.size

    while (width > (img.size[0] - 3*innerBorder) or heigth > (img.size[1] - 2.5*innerBorder)+titleActualSize[1]):
        font_size -= 1
        font = ImageFont.truetype('font/Font.ttf', size=font_size)
        new_size = font.getsize_multiline(text, spacing=4, stroke_width=2)
        width = new_size[0]
        heigth = new_size[1]
        # print(new_size)

    draw.multiline_text((innerBorder + 20, innerBorder + 50 + titleActualSize[1]), text, fill="Black", font=font)

    # --== FINISH GENERATING THE ACTUAL TEXT-BLOCK ==--

    if tmp:
        img.save('tmp/temp.jpg')
    else:
        img.save(f'output/{fileToCreate}.jpg')
        img.show()
