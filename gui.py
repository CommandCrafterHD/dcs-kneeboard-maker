from os import listdir
from os import mkdir
from generator import generatePage
from kivy.config import Config
Config.set('graphics', 'resizable', False)
from kivy import require
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager, Screen

require('1.11.1')

# #1D1F21 - 29  31  33 - Background
# #373B41 - 55  59  65 - Buttons/Foreground
# #282A2E - 40  42  46 - Shadows
# #8C9440 - 140 148 64 - Confirm
# #A54242 - 165 66  66 - Abort
# ALWAYS DIVIDE COLORS WITH 255


try:
    tmpExists = listdir('tmp')
    if not tmpExists:
        ExampleFile = open('input/Example.txt').readlines()
        generatePage('', ''.join(ExampleFile[1:]), ExampleFile[0], False, True)
except FileNotFoundError:
    mkdir('tmp')
    ExampleFile = open('input/Example.txt').readlines()
    generatePage('', ''.join(ExampleFile[1:]), ExampleFile[0], False, True)


class SelectScreen(Screen):
    pass


class LoadTextFromFile(Screen):

    def GetInputFiles(self):
        if listdir('input/'):
            return [x[:-4] for x in listdir('input/')]
        else:
            return "No Files Found!"

    def GetContentOfFirstFile(self):
        if listdir('input/'):
            return open('input/' + listdir('input/')[0], 'r').read()
        else:
            return "No Files Found!\n" \
                   "You should create some!"

    def Generate(self, fileName, currentText, unconventional):
        if fileName:
            file = open('input/' + fileName + '.txt', 'w+')
            file.write(currentText)
            file.close()
            lines = open('input/' + fileName + '.txt', 'r').readlines()
            generatePage(fileToCreate=fileName, title=lines[0], text=''.join(lines[1:]), unconventional=unconventional, tmp=False)

    def FileSelected(self, selectedFile, unconventional):
        file = open('input/' + selectedFile + '.txt').readlines()
        generatePage(fileToCreate=selectedFile, title=file[0], text=''.join(file[1:]), unconventional=unconventional, tmp=True)

    def ImgToTxt(self, currentlySelected):
        return open('input/' + currentlySelected + '.txt', 'r').read()

    def TxtToImg(self, currentlySelected, currentText, unconventional):
        file = open('input/' + currentlySelected + '.txt', 'w+')
        file.write(currentText)
        file.close()
        lines = open('input/' + currentlySelected + '.txt', 'r').readlines()
        generatePage(fileToCreate=currentlySelected, title=lines[0], text=''.join(lines[1:]), unconventional=unconventional, tmp=True)



class CreateTextFromScratch(Screen):
    def UpdateImage(self, unconventional, text):
        if text:
            lines = text.split('\n')
        else:
            lines = ['This is the title!', 'The text starts here!']
        generatePage(fileToCreate=None, title=lines[0], text=''.join(lines[1:]), unconventional=unconventional, tmp=True)

    def Generate(self, fileName, currentText, unconventional):
        if fileName:
            file = open('input/' + fileName + '.txt', 'w+')
            file.write(currentText)
            file.close()
            lines = open('input/' + fileName + '.txt', 'r').readlines()
            generatePage(fileToCreate=fileName, title=lines[0], text=''.join(lines[1:]), unconventional=unconventional, tmp=False)

    def TxtToImg(self, fileName, currentText, unconventional):
        if fileName:
            file = open('input/' + fileName + '.txt', 'w+')
            file.write(currentText)
            file.close()
            lines = open('input/' + fileName + '.txt', 'r').readlines()
            generatePage(fileToCreate=None, title=lines[0], text=''.join(lines[1:]), unconventional=unconventional, tmp=True)


class LoadTableFromFile(Screen):
    pass


class CreateTableFromScratch(Screen):
    pass


class WindowManager(ScreenManager):
    pass


kv = Builder.load_file('files/gui.kv')


class KneeBoardBuilder(App):

    def build(self):
        return kv


if __name__ == '__main__':
    KneeBoardBuilder().run()
