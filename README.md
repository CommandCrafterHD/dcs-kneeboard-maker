# DCS Kneeboard Maker

This is a simple script I threw together in python to create Kneeboards for DCS.
You can use the command line interface to convert .txt files in the input folder
to kneeboard-jpg's in the output folder.

## How to create the source .txt files?
- Create a file in the [input](/input) folder ending with .txt
- The first line of the file is the title
- All the other lines in the file will be added below

## How to convert those files?
- Run the [main.py](main.py) file and follow the instructions
- First choose the file you want to convert
    - It will give you a list of all files with numbers, just type in the number you want
- After that choose if it is a conventional format (gray stripes) or if it isn't (orange stripes)

### Why isn't there any GUI?
I'm working on it, give me some time!

### How does it work?
Magic... and some  [PIL](https://pypi.org/project/Pillow/) scripts... but mostly magic!

### Got any Ideas or problems?
Just create an issue or a pull request if you're good with python!

(c) 2020 CommandCrafterHD