from os import listdir
from os import mkdir
from generator import generatePage
from files import colors as Colors

try:
    listdir('input')
except FileNotFoundError:
    print(f'{Colors.YELLOW}WARN: Folder \'input\' not found! Creating it...')
    mkdir('input')

try:
    listdir('output')
except FileNotFoundError:
    print(f'{Colors.YELLOW}WARN: Folder \'output\' not found! Creating it...')
    mkdir('output')

print(Colors.GREEN + '/'*50 + f'\n||{Colors.BLUE} Kneeboard Generator {Colors.RED}(c) Florian Cegledi 2020{Colors.GREEN} ||\n' + '/'*50 + f'{Colors.END}\n')

listedDir = listdir('input')

if len(listedDir) == 0:
    print(f'{Colors.RED}ERROR: You do not have any files in your {Colors.YELLOW}\'input\'{Colors.RED} folder!{Colors.END}')
    quit(404)

files = ''
for i in range(len(listedDir)):
    files += Colors.YELLOW + str(i+1) + f'{Colors.END}: {Colors.BLUE}' + listedDir[i][:-4] + f'{Colors.END}\n'


def getFileToCreate():
    print('All Files:\n\n' + files)
    fileToCreate = input('Choose the number of your file: ')
    try:
        int(fileToCreate)
    except:
        print('You need to choose a NUMBER within the given range!')
        return fileToCreate
    if 0 < int(fileToCreate) < len(listdir('input'))+1:
        return listedDir[int(fileToCreate)-1]
    else:
        print('You need to choose a number within the given range!')
        return getFileToCreate()


fileToCreate = getFileToCreate()


def isUnconventional():
    unconventional = input('\nIs this an unconventionel checklist?(Y/N): ')
    if unconventional.lower()[0] in ['y', 'n']:
        if unconventional.lower()[0] == 'y':
            return True
        else:
            return False
    else:
        return isUnconventional()

unconventional = isUnconventional()

if fileToCreate.endswith('.txt'):
    fileToCreate = fileToCreate[:-4]

file = open('input/' + fileToCreate + '.txt').readlines()

title = file[0]

print('\n' + Colors.GREEN + '/'*50 + '\n' + f'||{Colors.YELLOW} File{Colors.END}:{Colors.BLUE} {fileToCreate}' + ' ' * (50-(11+len(fileToCreate))) + f'{Colors.GREEN}||\n'
+ f'{Colors.GREEN}||{Colors.YELLOW} Title{Colors.END}:{Colors.BLUE} {title[:-1]}' + ' ' * (50-(12+len(title[:-1]))) + f'{Colors.GREEN}||\n'
f'{Colors.GREEN}||{Colors.YELLOW} Unconventional{Colors.END}: ' + (f'{Colors.GREEN}TRUE' if unconventional else f'{Colors.RED}FALSE') + f'{Colors.GREEN}' + ' ' * (50-(21+len(str(unconventional)))) + f'{Colors.GREEN}||\n'
      + '/'*50)

text = ''.join(file[1:])

generatePage(fileToCreate, text, title, unconventional, True)
